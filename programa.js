let operation;
let saveoperation;
let lastoperation;

function calculate(){
    let result = document.getElementById('result');
    let AllClean = document.getElementById('AllClean');
    let addition = document.getElementById('addition');
    let subtration = document.getElementById('subtration');
    let multiplication = document.getElementById('multiplication');
    let division = document.getElementById('division');
    let remainder = document.getElementById('remainder');
    let equal = document.getElementById('equal');
    let one = document.getElementById('one');
    let two = document.getElementById('two');
    let three = document.getElementById('three');
    let four = document.getElementById('four');
    let five = document.getElementById('five');
    let six = document.getElementById('six');
    let seven = document.getElementById('seven');
    let eigth = document.getElementById('eigth');
    let nine = document.getElementById('nine');
    let cero = document.getElementById('cero');

}

function one(){
    result.textContent= result.textContent + "1";
}
function two(){
    result.textContent= result.textContent + "2";
}
function three(){
    result.textContent= result.textContent + "3";
}
function four(){
    result.textContent= result.textContent + "4";
}
function five(){
    result.textContent= result.textContent + "5";
}
function six(){
    result.textContent= result.textContent + "6";
}
function seven(){
    result.textContent= result.textContent + "7";
}
function eigth(){
    result.textContent= result.textContent + "8";
}
function nine(){
    result.textContent= result.textContent + "9";
}
function cero(){
    result.textContent= result.textContent + "0";
}
function AllClean(){
    result.textContent= "";
    operation="";
    saveoperation=0;
    lastoperation=0;
}
function addition(){
    saveoperation= result.textContent;
    operation= "+";
    clean();
}
function subtraction(){
    saveoperation= result.textContent;
    operation= "-";
    clean();
}
function multiplication(){
    saveoperation= result.textContent;
    operation= "*";
    clean();
}
function division(){
    saveoperation= result.textContent;
    operation= "/";
    clean();
}
function remainder(){
    saveoperation= result.textContent;
    operation= "%";
    clean();
}
function equal(){
    lastoperation= result.textContent;
    solve();
}
function clean(){
    result.textContent="";
}
function solve(){
    let answer= 0;
    switch(operation){
        case "+":
            answer= parseFloat(saveoperation) + parseFloat(lastoperation);
            break;
        case "-":
            answer= parseFloat(saveoperation) - parseFloat(lastoperation);
            break;
        case "*":
            answer= parseFloat(saveoperation) * parseFloat(lastoperation);
            break;
        case "/":
            answer= parseFloat(saveoperation) / parseFloat(lastoperation);
            break;
        case "%":
            answer= parseFloat(saveoperation) % parseFloat(lastoperation);
            break;
    }
    AllClean();
    result.textContent= answer;
}